﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Threading.Tasks;
using WebStore.Models;

namespace WebStore
{
	public partial class Product : ContentPage
	{
		public Product ()
		{
			InitializeComponent ();
		}

		protected override async void OnAppearing() {
			base.OnAppearing ();
			await getAccount ();
		}

		public async Task<IRestResponse> getAccount() {
			RestClient client = new RestClient("http://127.0.0.1:58077");
			var request = new RestRequest("/api/Products", Method.GET);
			IRestResponse<List<ProductDTO>> result = await client.Execute<List<ProductDTO>>(request);

			listView.ItemsSource = result.Data;
			//listView.ItemTapped = 
			return result;
		}
	}
}

