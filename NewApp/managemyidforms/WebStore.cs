﻿using System;

using Xamarin.Forms;

using RestSharp;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Collections.Generic;

namespace WebStore
{
	public class App : Application
	{
		public App ()
		{
			MainPage = new NavigationPage(new Login() {
				Title = "WebStore"
			});
		}

		protected override void OnStart ()
		{
		}

		protected override void OnSleep ()
		{
		}

		protected override void OnResume ()
		{
		}
	}
}

