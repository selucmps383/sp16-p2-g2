﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Net.Http;

namespace WebStore
{
	public partial class Login : ContentPage
	{
		public Login ()
		{
			InitializeComponent ();
		}

		async public void OnButtonClicked(object sender, EventArgs args)
		{
			//count++;

			((Button)sender).Text = username.Text + password.Text;
				//String.Format("{0} click{1}!", count, count == 1 ? "" : "s");

			// Do some operations to login user
			var client = new RestClient(GlobalVariables.BaseUrl);
			var request = new RestRequest("api/ApiKey", Method.GET);
			request.AddParameter("email", username.Text);
			request.AddParameter("password", password.Text);
			try {
				var response = await client.Execute(request);
				await DisplayAlert ("alert", "success!", "close");
			} catch (HttpRequestException) {
				// If login was successfully completed
				await DisplayAlert ("alert", "failure!", "close");
			}
		}
	}
}

