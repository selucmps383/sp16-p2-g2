﻿(function() {
    var WebStoreApp = angular.module('WebStoreApp');
    WebStoreApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/browse');

        $stateProvider.state('browse', {
            url: '/browse',
            views: {
                list: {
                    templateUrl: 'Content/app/views/browse.html'
                }
            }
        })

        $stateProvider.state('cart', {
            url: '/cart',
            views: {
                list: {
                    templateUrl: 'Content/app/views/cart.html'
                }
            }
        })

        $stateProvider.state('checkout', {
            url: '/checkout',
            views: {
                list: {
                    templateUrl: 'Content/app/views/checkout.html'
                }
            }
        })

        $stateProvider.state('confirm', {
            url: '/confirm',
            views: {
                list: {
                    templateUrl: 'Content/app/views/confirm.html'
                }
            }
        })
    }]);
})()