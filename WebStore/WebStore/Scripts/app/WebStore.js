﻿(function () {
    var WebStoreApp = angular.module('WebStoreApp', ['ui.router', 'ui.bootstrap']);

    WebStoreApp.controller('ProductController', ['$scope', '$state', 'WebStoreService', function ($scope, $state, WebStoreService) {
        getProducts();
        function getProducts() {
            WebStoreService.getProducts()
                .success(function (products) {
                    $scope.products = products;
                    console.log($scope.products);
                })
                .error(function (error) {
                    $scope.status = 'Unable to load products: ' + error.message;
                    console.log($scope.status);
                });
        }

        $scope.sortItemsList = [
            'Category',
            'Manufacturer',
            'Name',
            'Price',
            //This one is last because it maps to quantity
            'InventoryCount',

        ];

        $scope.sortItemsListNames = {
            'Category': 'Category',
            'Manufacturer': 'Manufacturer',
            'Name': 'Name',
            'Price': 'Price',
            'InventoryCount': 'Quantity',
        }

        $scope.sortItemsField = 'Name';
        $scope.sortItems = function (field) {
            $scope.sortItemsField = field;
        };

        $scope.saleLoading = false;
        $scope.saleError = false;

        $scope.cart = {};
        $scope.cartTotalPrice = 0;
        $scope.cartTotalQuantity = 0;
        $scope.cartPreviewOpen = false;
        $scope.closeCartPreview = function () {
            $scope.cartPreviewOpen = false;
        };

        $scope.console = function (str) {
            console.log(str);
        }

        $scope.addToCart = function (r) {
            if (!(r.Id in $scope.cart)) {
                $scope.cart[r.Id] = angular.copy(r);
            }
            if ('cartQuantity' in $scope.cart[r.Id]) {
                $scope.cart[r.Id].cartQuantity += r.addQuantity;
            } else {
                $scope.cart[r.Id].cartQuantity = r.addQuantity;
            }
            r.addQuantity = 1;
            //Needed so the item total updates in the navbar
            $scope.updateCartInfo();
        }

        $scope.removeFromCart = function (itemId) {
            console.log(itemId, $scope.cart[itemId]);
            delete $scope.cart[itemId].cartQuantity;
            delete $scope.cart[itemId];
            console.log($scope.cart);
            //Needed so the item total updates in the navbar
            $scope.updateCartInfo();
        }

        $scope.updateCartInfo = function () {
            var cart = $scope.cart;
            var cartTotalPrice = 0;
            var cartTotalQuantity = 0;
            angular.forEach(cart, function (value, key) {
                cartTotalPrice += value.Total;
                cartTotalQuantity += value.cartQuantity;
            });
            $scope.cartTotalPrice = cartTotalPrice;
            $scope.cartTotalQuantity = cartTotalQuantity;
            return cartTotalPrice;
        }

        $scope.checkout = function (email) {
            $scope.saleLoading = true;
            $scope.saleError = false;
            WebStoreService.checkout($scope.cart, email, function (success) {
                $scope.saleLoading = false;
                if (success) {
                    //Clear cart
                    $scope.cart = {};
                    $scope.cartTotalPrice = 0;
                    $scope.cartTotalQuantity = 0;
                    $state.go('confirm');
                } else {
                    $scope.saleError = true;
                }
            });

        }

        $scope.objLength = function (obj) {
            if (typeof obj === 'undefined') {
                return 0;
            }
            return Object.keys(obj).length
        }

        $scope.itemPhotoUrl = function (name) {
            return "https://placehold.it/150x150?text=" + name;
        }

        //Based off of http://stackoverflow.com/a/25900981
        //Needed to make the whole <li> selectable, not just the textbox
        $scope.rowClicked = function (prop, opt) {
            $scope.filter[prop][opt] = !$scope.filter[prop][opt];
        };

        $scope.toggleCheckboxSelection = function ($event) {
            $event.stopPropagation();
        }

        //From http://stackoverflow.com/a/2398350
        $scope.filter = {};
        $scope.filterList = ['Category', 'Manufacturer']

        $scope.getOptionsFor = function (propName) {
            return ($scope.products || []).map(function (w) {
                return w[propName];
            }).filter(function (w, idx, arr) {
                return arr.indexOf(w) === idx;
            });
        };

        $scope.filterByProperties = function (products) {
            // Use this snippet for matching with AND
            var matchesAND = true;
            for (var prop in $scope.filter) {
                if (noSubFilter($scope.filter[prop])) continue;
                if (!$scope.filter[prop][products[prop]]) {
                    matchesAND = false;
                    break;
                }
            }
            return matchesAND;
        };

        function noSubFilter(subFilterObj) {
            for (var key in subFilterObj) {
                if (subFilterObj[key]) return false;
            }
            return true;
        }
    }]);

    WebStoreApp.factory('WebStoreService', ['$http', function ($http) {

        var WebStoreService = {};
        //Used to precompile the email template, else checkout will take 20+ seconds the first time
        $http.get('http://localhost:58077/Emails/Email');
        WebStoreService.getProducts = function () {
            return $http.get('/api/Products');
            //return $http.get('https://api.myjson.com/bins/1qk2l');
        };

        WebStoreService.checkout = function (cart, email, callback) {
            var reqCart = [];
            var qty = [];


            angular.forEach(cart, function (value, key) {
                console.log(value, key);
                reqCart.push({
                    Id: value.Id
                });
                qty.push({
                    Quantity: value.cartQuantity
                });
            });
            console.log(reqCart, qty)

            var req = {
                "EmailAddress": email,
                "Products": reqCart,
                "Quantity": qty
            }
            $http.post('/api/Sales', req).success(function success(response) {
                console.log(response);
                callback(true);
            }, function error(error) {
                console.log('error:', error);
                callback(false);
            });
        }
        return WebStoreService;
    }]);
})()