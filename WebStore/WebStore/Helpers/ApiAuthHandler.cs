﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using WebStore.Controllers;

namespace WebStore.Helpers
{
    public class ApiAuthHandler : DelegatingHandler
    {
        private const string AUTH_ID_HEADER = "xcmps383authenticationid";
        private const string AUTH_KEY_HEADER = "xcmps383authenticationkey";

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            bool isValidApiKey = false;
            string userId = "";

            // Check for both required headers
            if (request.Headers.Contains(AUTH_ID_HEADER) && request.Headers.Contains(AUTH_KEY_HEADER))
            {
                userId = request.Headers.GetValues(AUTH_ID_HEADER).First();
                var apiKey = request.Headers.GetValues(AUTH_KEY_HEADER).First();
                isValidApiKey = new UsersController().IsValidApiLogin(userId, apiKey);
            }

            if (isValidApiKey)
            {
                var identity = new GenericIdentity(userId);
                var principal = new GenericPrincipal(identity, null);

                Thread.CurrentPrincipal = principal;
                if (HttpContext.Current != null)
                {
                    HttpContext.Current.User = principal;
                }
            }
            return await base.SendAsync(request, cancellationToken);
        }
    }
}