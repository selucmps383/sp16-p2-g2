/*These values are replaced by the server*/
    window.swashbuckleConfig = {
    rootUrl: '%(RootUrl)',
    discoveryPaths: arrayFrom('%(DiscoveryPaths)'),
    booleanValues: arrayFrom('%(BooleanValues)'),
    validatorUrl: stringOrNullFrom('%(ValidatorUrl)'),
    customScripts: arrayFrom('%(CustomScripts)'),
    docExpansion: '%(DocExpansion)',
    oAuth2Enabled: ('%(OAuth2Enabled)' == 'true'),
    oAuth2ClientId: '%(OAuth2ClientId)',
    oAuth2Realm: '%(OAuth2Realm)',
    oAuth2AppName: '%(OAuth2AppName)'
    }
