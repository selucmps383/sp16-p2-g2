﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebStore.Models
{
    public class Sale
    {
        [Key]
        public int Id { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        [DataType(DataType.Currency)]
        [DisplayName("Total Amount")]
        public decimal TotalAmount { get; set; }

        [DataType(DataType.EmailAddress)]
        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        public virtual ICollection<QuantityModel> Quantity { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}