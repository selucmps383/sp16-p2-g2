﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebStore.Models
{
    public class ProductDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public decimal Price { get; set; }
        public int InventoryCount { get; set; }
        public int? CategoryId { get; set; }
        public int? ManufacturerId { get; set; }
        public string Category { get; set; }
        public string Manufacturer { get; set; }
    }
}