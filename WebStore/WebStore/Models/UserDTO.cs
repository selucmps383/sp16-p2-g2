﻿using System;
using System.Linq;
using System.Web;

namespace WebStore.Models
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string EmailAddress { get; set; }
    }
}