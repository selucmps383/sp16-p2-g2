﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebStore.Models
{
    public class QuantityModel
    {
        [Key]
        public int Id { get; set; }

        [Range(0, int.MaxValue)]
        public int Quantity { get; set; }
    }
}