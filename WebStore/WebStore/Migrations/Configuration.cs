namespace WebStore.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Web.Helpers;

    internal sealed class Configuration : DbMigrationsConfiguration<WebStoreContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(WebStoreContext context)
        {
            var Users = new List<User>()
            {
                new User()
                {
                    EmailAddress = "sa@383.com",
                    Password = Crypto.HashPassword("password"),
                    ApiKey = GetApiKey()
                },
                new User()
                {
                    EmailAddress = "admin@383.com",
                    Password = Crypto.HashPassword("selu2016"),
                    ApiKey = GetApiKey()
                },
                new User()
                {
                    EmailAddress = "user@383.com",
                    Password = Crypto.HashPassword("selu2016"),
                    ApiKey = GetApiKey()
                }
            };
            foreach (User user in Users)
            {
                user.ConfirmPassword = user.Password;
                context.Users.AddOrUpdate(user);
            }

            var AwesomeStuffCategory = new Category() { Name = "Awesome Stuff" };
            var OkStuffCategory = new Category() { Name = "Ok Stuff" };
            context.Categories.AddOrUpdate(AwesomeStuffCategory, OkStuffCategory);

            var SomeDudeManufacturer = new Manufacturer() { Name = "Some Dude" };
            var AnotherDudeManufacturer = new Manufacturer() { Name = "Another Dude" };
            context.Manufacturers.AddOrUpdate(SomeDudeManufacturer, AnotherDudeManufacturer);

            var WidgetProduct = new Product()
            {
                Name = "Widget",
                Price = 10,
                InventoryCount = 100,
                CreatedDate = DateTime.UtcNow,
                LastModifiedDate = DateTime.UtcNow,
                Category = AwesomeStuffCategory,
                Manufacturer = SomeDudeManufacturer
            };
            var CogProduct = new Product()
            {
                Name = "Cog",
                Price = 1,
                InventoryCount = 1000,
                CreatedDate = DateTime.UtcNow,
                LastModifiedDate = DateTime.UtcNow,
                Category = OkStuffCategory,
                Manufacturer = AnotherDudeManufacturer
            };
            var SprocketProduct = new Product()
            {
                Name = "Sprocket",
                Price = 5,
                InventoryCount = 50,
                CreatedDate = DateTime.UtcNow,
                LastModifiedDate = DateTime.UtcNow,
                Category = OkStuffCategory,
                Manufacturer = SomeDudeManufacturer
            };
            context.Products.AddOrUpdate(WidgetProduct, CogProduct, SprocketProduct);

            var FirstSale = new Sale()
            {
                Date = DateTime.UtcNow,
                TotalAmount = 15,
                EmailAddress = "johnsmith@site.com",
                Products = new List<Product>() { WidgetProduct, SprocketProduct },
                Quantity = new List<QuantityModel>()
                {
                    new QuantityModel() { Quantity = 1 },
                    new QuantityModel() { Quantity = 2 },
                }
            };
            context.Sales.AddOrUpdate(FirstSale);

            context.SaveChanges();
        }

        private static string GetApiKey()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[16];
                rng.GetBytes(bytes);
                return Convert.ToBase64String(bytes);
            }
        }
    }
}
