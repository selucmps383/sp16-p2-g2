﻿using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebStore.Controllers
{
    public class EmailsController : Controller
    {
        // GET: Emails
        public ActionResult Email()
        {
            try {
                //Only used to pre-compile the view, we don't need to actually render it
                View();
            } catch (RuntimeBinderException) { }
            return new EmptyResult();
        }
    }
}