﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using WebStore.Models;
using WebStore.Helpers;
using System.Web.Helpers;
using System.Data.Entity;

namespace WebStore.Controllers
{
    public class ApiKeyController : ApiController
    {
        private WebStoreContext db = new WebStoreContext();

        [HttpGet]
        public HttpResponseMessage Index(string email, string password)
        {
            if (email == null || password == null || !Utility.IsValidEmail(email))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Malformed Input.");
            }

            User user = db.Users.FirstOrDefault(u => u.EmailAddress == email);
			
            if (user != null && Crypto.VerifyHashedPassword(user.Password, password))
            {
                if (user.ApiKey == null)
                {
                    user.ApiKey = GetApiKey();
                    db.Entry(user).State = EntityState.Modified;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception){ }
                    return Request.CreateResponse(HttpStatusCode.OK, user.ApiKey);	
                }
                return Request.CreateResponse(HttpStatusCode.OK, user.ApiKey);
            }    
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Email / password incorrect.");
        }

        private static string GetApiKey()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[16];
                rng.GetBytes(bytes);
                return Convert.ToBase64String(bytes);
            }
        }
    }
}
