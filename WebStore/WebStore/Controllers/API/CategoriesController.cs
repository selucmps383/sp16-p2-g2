﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebStore.Models;

namespace WebStore.Controllers
{
    public class CategoriesController : ApiController
    {
        private WebStoreContext db = new WebStoreContext();

        // GET: api/Categories
        public IList<CategoryDTO> GetCategories()
        {
            return db.Categories.Select(c => new CategoryDTO
            {
                Id = c.Id,
                Name = c.Name,
                Products = c.Products.ToList()
            }).ToList();
        }

        // GET: api/Categories/5
        [ResponseType(typeof(CategoryDTO))]
        public IHttpActionResult GetCategory(int id)
        {
            var category = db.Categories.Include(c => c.Id).Select(
                    c => new CategoryDTO()
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Products = c.Products.ToList()
                    }).SingleOrDefault(c => c.Id == id);

            if (category == null)
            {
                return NotFound();
            }
            return Ok(category);
        }

        // PUT: api/Categories/5
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCategory(int id, Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != category.Id)
            {
                return BadRequest();
            }

            db.Entry(category).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Categories
        [Authorize]
        [ResponseType(typeof(Category))]
        public IHttpActionResult PostCategory(Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IEnumerable<int> productIds = category.Products.Select(p => p.Id);
            category.Products = db.Products.Where(p => productIds.Contains(p.Id)).ToList();

            db.Categories.Add(category);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = category.Id }, category);
        }

        // DELETE: api/Categories/5
        [Authorize]
        [ResponseType(typeof(Category))]
        public IHttpActionResult DeleteCategory(int id)
        {
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return NotFound();
            }

            for (int i = 0; i < category.Products.Count; i++)
            {
                var product = category.Products.ElementAt(i);
                product.CategoryId = null;
                product.Category = null;
                db.Entry(product).State = EntityState.Modified;
            }
            db.Categories.Remove(category);
            db.SaveChanges();

            return Ok(category);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return db.Categories.Count(e => e.Id == id) > 0;
        }
    }
}