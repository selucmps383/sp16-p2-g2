﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebStore.Models;

namespace WebStore.Controllers
{
    public class ManufacturersController : ApiController
    {
        private WebStoreContext db = new WebStoreContext();

        // GET: api/Manufacturers
        public IList<ManufacturerDTO> GetManufacturers()
        {
            return db.Manufacturers.Select(m => new ManufacturerDTO
            {
                Id = m.Id,
                Name = m.Name,
                Products = m.Products.ToList()
            }).ToList();
        }

        // GET: api/Manufacturers/5
        [ResponseType(typeof(ManufacturerDTO))]
        public IHttpActionResult GetManufacturer(int id)
        {
            var man = db.Manufacturers.Include(m => m.Id).Select(
                    m => new ManufacturerDTO()
                    {
                        Id = m.Id,
                        Name = m.Name,
                        Products = m.Products.ToList()
                    }).SingleOrDefault(m=> m.Id == id);

            if (man == null)
            {
                return NotFound();
            }
            return Ok(man);

        }

        // PUT: api/Manufacturers/5
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutManufacturer(int id, Manufacturer manufacturer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != manufacturer.Id)
            {
                return BadRequest();
            }
            manufacturer.Products = null;
            db.Entry(manufacturer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ManufacturerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Manufacturers
        [Authorize]
        [ResponseType(typeof(Manufacturer))]
        public IHttpActionResult PostManufacturer(Manufacturer manufacturer)
        {
            manufacturer.Products = null;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IEnumerable<int> productIds = manufacturer.Products.Select(p => p.Id);
            manufacturer.Products = db.Products.Where(p => productIds.Contains(p.Id)).ToList();

            db.Manufacturers.Add(manufacturer);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = manufacturer.Id }, manufacturer);
        }

        // DELETE: api/Manufacturers/5
        [Authorize]
        [ResponseType(typeof(Manufacturer))]
        public IHttpActionResult DeleteManufacturer(int id)
        {
            Manufacturer manufacturer = db.Manufacturers.Find(id);
            if (manufacturer == null)
            {
                return NotFound();
            }

            for (int i = 0; i < manufacturer.Products.Count; i++)
            {
                var product = manufacturer.Products.ElementAt(i);
                product.ManufacturerId = null;
                product.Manufacturer = null;
                db.Entry(product).State = EntityState.Modified;
            }

            db.Manufacturers.Remove(manufacturer);
            db.SaveChanges();

            return Ok(manufacturer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ManufacturerExists(int id)
        {
            return db.Manufacturers.Count(e => e.Id == id) > 0;
        }
    }
}