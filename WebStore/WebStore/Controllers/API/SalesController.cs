﻿using Mailgun.Messages;
using Mailgun.Service;
using Postal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebStore.Models;

namespace WebStore.Controllers
{
    public class SalesController : ApiController
    {
        private WebStoreContext db = new WebStoreContext();

        // GET: api/Sales
        public IQueryable<Sale> GetSales()
        {
            return db.Sales;
        }

        // GET: api/Sales/5
        [ResponseType(typeof(Sale))]
        public IHttpActionResult GetSale(int id)
        {
            Sale sale = db.Sales.Find(id);
            if (sale == null)
            {
                return NotFound();
            }

            return Ok(sale);
        }

        // PUT: api/Sales/5
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSale(int id, Sale sale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sale.Id)
            {
                return BadRequest();
            }

            db.Entry(sale).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sales
        [ResponseType(typeof(Sale))]
        async public Task<IHttpActionResult> PostSale(Sale sale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IEnumerable<int> productIds = sale.Products.Select(p => p.Id);
            sale.Products = db.Products.Where(p => productIds.Contains(p.Id)).ToList();

            //TODO: Add quantity support
            //sale.TotalAmount = sale.Products.Sum(p => p.Price);
            
            sale.TotalAmount = 0;

            foreach (var obj in sale.Products.Zip(sale.Quantity, Tuple.Create))
            {
                obj.Item1.InventoryCount -= obj.Item2.Quantity;
            }

            /*
            Zip combines the two objects to loop over them as one
            sale.Products is in obj.Item1
            sale.Quantity is in obj.Item2
            */
            var MergedProducts = sale.Products.Zip(sale.Quantity, Tuple.Create);
            sale.TotalAmount = MergedProducts.Sum(p => p.Item1.Price * p.Item2.Quantity);

            sale.Date = DateTime.UtcNow;

            db.Sales.Add(sale);
            db.SaveChanges();

            //Generate Email
            //"Email" is a view located at Views/Emails/Email.cshtml
            dynamic email = new Email("Email");
            email.To = sale.EmailAddress;
            email.From = "383@sandbox1edcc50c23f04e338fd3b17f2d838959.mailgun.org";
            email.Sale = sale;
            email.MergedProducts = MergedProducts;
            IEmailService emailService = new Postal.EmailService();
            System.Net.Mail.MailMessage message = emailService.CreateMailMessage(email);

            var pm = new PreMailer.Net.PreMailer(message.Body, new Uri("http://localhost:58077/"));
            var premailedOutput = pm.MoveCssInline(stripIdAndClassAttributes: true, removeStyleElements: true);

            //This API key will expire within a few days after the presentation
            var mg = new MessageService("key-d26e98a20ebc2ee6fcce51bac8d305bb");

            //Get ready to send email
            var msg = new MessageBuilder()
                 .AddToRecipient(new Recipient
                 {
                     Email = email.To,
                 })
                 //.SetTestMode(true)
                 .SetSubject("Your order #" + sale.Id)
                 .SetFromAddress(new Recipient { Email = email.From, DisplayName = "Web Store" })
                 //.SetTextBody("This is a test")
                 .SetHtmlBody(premailedOutput.Html)
                 .GetMessage();

            //Send Email
            var content = await mg.SendMessageAsync("sandbox1edcc50c23f04e338fd3b17f2d838959.mailgun.org", msg);
            //content.IsSuccessStatusCode();

            return CreatedAtRoute("DefaultApi", new { id = sale.Id }, sale);
        }

        // DELETE: api/Sales/5
        [Authorize]
        [ResponseType(typeof(Sale))]
        public IHttpActionResult DeleteSale(int id)
        {
            Sale sale = db.Sales.Find(id);
            if (sale == null)
            {
                return NotFound();
            }

            db.Sales.Remove(sale);
            db.SaveChanges();

            return Ok(sale);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SaleExists(int id)
        {
            return db.Sales.Count(e => e.Id == id) > 0;
        }
    }
}