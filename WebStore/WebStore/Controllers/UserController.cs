﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using WebStore.Models;

namespace WebStore.Controllers
{
    public class UserController : Controller
    {
        private WebStoreContext db = new WebStoreContext();

        // GET: User/Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        // POST: User/Login
        [HttpPost]
        public ActionResult Login(string emailAddress, string password)
        {
            User user = db.Users.FirstOrDefault(u => u.EmailAddress == emailAddress);

            if (user != null)
            {
                if (Crypto.VerifyHashedPassword(user.Password, password))
                {
                    FormsAuthentication.SetAuthCookie(user.EmailAddress, createPersistentCookie: false);
                    return RedirectToAction("Index", "Home");
                }
            }

            return View(user);
        }

        // GET: User/Logout
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        // GET: User
        [Authorize]
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: User/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "EmailAddress,Password,ConfirmPassword")] User user)
        {
            if (ModelState.IsValid)
            {
                // Check for an existing user with that email address
                User existingUser = db.Users.FirstOrDefault(u => u.EmailAddress == user.EmailAddress);
                if (existingUser != null)
                {
                    ModelState.AddModelError("", "Unable to create user. A user with that email address already exists.");
                    return View(user);
                }

                // URL used to generate an API key for the user
                var uri = $"{Request.Url.Scheme}://{Request.Url.Authority}/api/ApiKey?email={user.EmailAddress}&password={user.Password}";

                user.Password = Crypto.HashPassword(user.Password);
                user.ConfirmPassword = user.Password;

                db.Users.Add(user);
                db.SaveChanges();

                // Make a request to ApiKeyController
                using (var client = new HttpClient())
                {
                    await client.GetAsync(uri);
                }

                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: User/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,EmailAddress,Password,ConfirmPassword,ApiKey")] User user)  // Is this secure???
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                user.Password = Crypto.HashPassword(user.Password);
                user.ConfirmPassword = user.Password;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: User/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
