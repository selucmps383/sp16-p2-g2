﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebStore.Models;

namespace WebStore.Controllers
{
    public class HomeController : Controller
    {
        private WebStoreContext db = new WebStoreContext();

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [Authorize]
        public ActionResult API()
        {
            User user = db.Users.FirstOrDefault(u => u.EmailAddress == User.Identity.Name);
            ViewBag.ApiKey = user.ApiKey;
            return View();
        }
    }
}
