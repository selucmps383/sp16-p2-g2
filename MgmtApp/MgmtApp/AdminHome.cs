using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MgmtApp
{
    [Activity(Label = "AdminHome")]
    public class AdminHome : Activity
    {

        private List<Product> Products;

        private ListView ListView;
        private EditText search;
        private LinearLayout container;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            ListView = FindViewById<ListView>(Resource.Id.listView);
            search = FindViewById<EditText>(Resource.Id.etSearch);
            container = FindViewById<LinearLayout>(Resource.Id.llContainer);


            search.Alpha = 0;
            Products = new List<Product>();

            // Create your application here
        }
    }
}