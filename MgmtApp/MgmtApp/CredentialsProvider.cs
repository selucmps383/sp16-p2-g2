using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LoginScreen;
using RestSharp;

namespace MgmtApp
{
    public class CredentialsProvider : ICredentialsProvider
    {
        // Constructor without parameters is required

        public bool NeedLoginAfterRegistration
        {
            get
            {
                // If you want your user to login after he/she has been registered
                return true;

                // Otherwise you can:
                // return false;
            }
        }

        public void Login(string userName, string password, Action successCallback, Action<LoginScreenFaultDetails> failCallback)
        {
            // Do some operations to login user
            
            // ATTENTION: Replace this IP with the IP of the host!
            var client = new RestClient(GlobalVariables.BaseUrl);
            var request = new RestRequest("api/ApiKey", Method.GET);
            request.AddParameter("email", userName);
            request.AddParameter("password", password);
            var response = client.Execute(request);

            // If login was successfully completed
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                successCallback();
            }
            else
            {
                // Otherwise
                failCallback(new LoginScreenFaultDetails
                {
                    CommonErrorMessage = "Username or password is incorrect"
                });
            }
        }

        public void Register(string email, string userName, string password, Action successCallback, Action<LoginScreenFaultDetails> failCallback)
        {
            // Do some operations to register user

            // If registration was successfully completed
            successCallback();

            // Otherwise
            // failCallback(new LoginScreenFaultDetails {
            //  CommonErrorMessage = "Some error message relative to whole form",
            //  EmailErrorMessage = "Some error message relative to e-mail form field",
            //  UserNameErrorMessage = "Some error message relative to user name form field",
            //  PasswordErrorMessage = "Some error message relative to password form field"
            // });
        }

        public void ResetPassword(string email, Action successCallback, Action<LoginScreenFaultDetails> failCallback)
        {
            // Do some operations to reset user's password

            // If password was successfully reset
            successCallback();

            // Otherwise
            // failCallback(new LoginScreenFaultDetails {
            //  CommonErrorMessage = "Some error message relative to whole form",
            //  EmailErrorMessage = "Some error message relative to e-mail form field"
            // });
        }

        public bool ShowPasswordResetLink
        {
            get
            {
                // If you want your login screen to have a forgot password button
                //return true;

                // Otherwise you can:
                return false;
            }
        }

        public bool ShowRegistration
        {
            get
            {
                // If you want your login screen to have a register new user button
                //return true;

                // Otherwise you can:
                return false;
            }
        }
    }
}