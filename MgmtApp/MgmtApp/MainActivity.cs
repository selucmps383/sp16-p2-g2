﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using LoginScreen;

namespace MgmtApp
{
    [Activity(Label = "MgmtApp", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        
        //Button btnLogin;
       

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            LoginScreenControl<CredentialsProvider>.Activate(this);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
          //  btnLogin = FindViewById<Button>(Resource.Id.buttonLogIn);
          //  btnLogin.Click += btnLogin_Click;
        
        }
        void btnLogin_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this,typeof(AdminHome));
            this.StartActivity(intent);
        }
    }
}

