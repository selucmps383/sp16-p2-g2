using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
namespace MgmtApp
{
    class ProductsAdapter : BaseAdapter<Product>
    {
        private Context Context;
        private int RowLayout;
        private List<Product> Products;
        private int[] mAlternatingColors;

        public ProductsAdapter(Context context, int rowLayout, List<Product> products)
        {
            Context = context;
            RowLayout = rowLayout;
            Products = products;
            mAlternatingColors = new int[] { 0xF2F2F2, 0x009900 };
        }

        public override int Count
        {
            get { return Products.Count; }
        }

        public override Product this[int position]
        {
            get { return Products[position]; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View row = convertView;

            if (row == null)
            {
                row = LayoutInflater.From(Context).Inflate(RowLayout, parent, false);
            }

            row.SetBackgroundColor(GetColorFromInteger(mAlternatingColors[position % mAlternatingColors.Length]));


            TextView productName = row.FindViewById<TextView>(Resource.Id.txtProductName);
            productName.Text = Products[position].Name;

            TextView manufacturerName = row.FindViewById<TextView>(Resource.Id.txtManufacturerName);
            manufacturerName.Text = Products[position].Manufacturer.Name;

            TextView quantity = row.FindViewById<TextView>(Resource.Id.txtQty);
            quantity.Text = Products[position].InventoryCount.ToString();

            TextView price = row.FindViewById<TextView>(Resource.Id.txtPrice);
            price.Text = Products[position].Price.ToString();


            if ((position % 2) == 1)
            {
                //Green background, set text white
                productName.SetTextColor(Color.White);
                manufacturerName.SetTextColor(Color.White);
                quantity.SetTextColor(Color.White);
                
            }

            else
            {
                //White background, set text black
                productName.SetTextColor(Color.Black);
                manufacturerName.SetTextColor(Color.Black);
                quantity.SetTextColor(Color.Black);
                
            }

            return row;
        }

        private Color GetColorFromInteger(int color)
        {
            return Color.Rgb(Color.GetRedComponent(color), Color.GetGreenComponent(color), Color.GetBlueComponent(color));
        }
    }
}